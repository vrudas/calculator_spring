package calculator.util;

import calculator.entity.User;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

/**
 * Created by Vasiliy on 11.02.14.
 */
public class LoginService {
    private static final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    public LoginService() {

    }

    public boolean verifyUserNameAndPassword(String name, String password) {
        Session session = sessionFactory.openSession();
        Query whereClause = session.createQuery("FROM User u WHERE u.name=?  AND u.password=?");
        whereClause.setParameter(0, name);
        whereClause.setParameter(1, password);
        boolean userStatus = false;
        try {
            @SuppressWarnings("unchecked")
            List<User> userList = whereClause.list();
            if (userList.size() != 0) {
                userStatus = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userStatus;
    }

}
