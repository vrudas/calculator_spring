package calculator.controller;

import calculator.main.Calculator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller

public class CalculateController {
    @Autowired
    private Calculator calculator = new Calculator();

    @RequestMapping(value = "/calculator", method = RequestMethod.GET)
    public String view() {
        return "calculator";
    }

    @RequestMapping(value = "/calculator.form", method = RequestMethod.GET)
    public
    @ResponseBody
    String digit(@RequestParam("button") String button, Model model) {
        if ((button.charAt(0) >= '0') && (button.charAt(0) <= '9')) {
            calculator.digit(Integer.parseInt("" + button));
        } else {
            switch (button) {
                case "+":
                    calculator.setOperation("+");
                    break;
                case "-":
                    calculator.setOperation("-");
                    break;
                case "*":
                    calculator.setOperation("*");
                    break;
                case "/":
                    calculator.setOperation("/");
                    break;
                case "C":
                    calculator.setOperation("C");
                case "=":
                    calculator.evaluate();
                    break;
                default:
                    break;
            }
        }
        return calculator.getResult();
    }
}