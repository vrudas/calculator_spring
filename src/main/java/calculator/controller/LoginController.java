package calculator.controller;

import calculator.util.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Vasiliy on 11.02.14.
 */
@Controller
public class LoginController {
    @Autowired
    private LoginService loginService;

    @RequestMapping(value = "/")
    public String view() {
        return "login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ModelAndView processCredentials(@RequestParam("name") String name, @RequestParam("password") String password) {
        String message = "Invalid credentials";
        if (loginService.verifyUserNameAndPassword(name, password)) {
            message = "Welcome " + name + "!";
            return new ModelAndView("calculator", "message", message);
        }
        return new ModelAndView("login", "message", message);
    }

    public LoginService getLoginService() {
        return loginService;
    }

    public void setLoginService(LoginService loginService) {
        this.loginService = loginService;
    }
}
