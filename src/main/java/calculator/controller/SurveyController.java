package calculator.controller;

import calculator.main.Question;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vasiliy on 09.02.14.
 */
@Controller
public class SurveyController {
    List<Question> questions = new ArrayList<>();

    @RequestMapping(value = "/survey", method = RequestMethod.GET)
    public String survey(Model model) {
        questions.add(new Question(Question.Type.QUESTION, "1. What is you name?", null));
        model.addAttribute("questions", questions);
        return "survey";
    }

        /*
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();

        User user = new User();
        user.setName("Catwoman");
        user.setPassword("Celina");
        session.save(user);

        session.getTransaction().commit();

        */
}
