package calculator.main;

import java.util.List;

/**
 * Created by Vasiliy on 09.02.14.
 */
public class Question {
    public static enum Type {TEXT, QUESTION, RADIO, CHECKBOX}

    ;

    public Type questionType;
    public String qusetionName;
    public List<String> questionAnswers;


    public Question(Type questionType, String qusetionName, List<String> questionAnswers) {
        this.questionType = questionType;
        this.qusetionName = qusetionName;
        this.questionAnswers = questionAnswers;
    }

    public Type getQuestionType() {
        return questionType;
    }

    public void setQuestionType(Type questionType) {
        this.questionType = questionType;
    }

    public String getQusetionName() {
        return qusetionName;
    }

    public void setQusetionName(String qusetionName) {
        this.qusetionName = qusetionName;
    }

    public List<String> getQuestionAnswers() {
        return questionAnswers;
    }

    public void setQuestionAnswers(List<String> questionAnswers) {
        this.questionAnswers = questionAnswers;
    }
}
