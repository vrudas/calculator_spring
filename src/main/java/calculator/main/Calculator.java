package calculator.main;

import org.springframework.stereotype.Service;

/**
 * Created by Vasiliy on 04.02.14.
 */
@Service
public class Calculator {
    public static final Integer TEN = 10;
    public static final Integer ZERO = 0;

    private Integer lastNumber = ZERO;

    private Integer currentNumber;

    private Integer memoryNumber;

    private String operation;

    public void digit(int digit) {
        if ((digit < 0) || (digit > 9)) {
            throw new IllegalArgumentException("Invalid digit: " + digit);
        }
        if (currentNumber != null) {
            currentNumber = currentNumber * TEN + (digit);
        } else {
            currentNumber = digit;
        }
    }

    public void evaluate() {
        if (operation != null) {
            switch (operation) {
                case "+":
                    lastNumber = lastNumber + (currentNumber != null ? currentNumber : lastNumber);
                    break;
                case "-":
                    lastNumber = lastNumber - (currentNumber != null ? currentNumber : lastNumber);
                    break;
                case "*":
                    lastNumber = lastNumber * (currentNumber != null ? currentNumber : lastNumber);
                    break;
                case "/":
                    try {
                        lastNumber = lastNumber / (currentNumber != null ? currentNumber : lastNumber);
                    } catch (Exception e) {
                        lastNumber = ZERO;
                    }
                    break;
                case "C":
                    currentNumber = null;
                    lastNumber = ZERO;
                    operation = null;
                    break;
            }
        } else if (currentNumber != null) {
            lastNumber = currentNumber;
        }
        currentNumber = null;
        operation = null;
    }

    public String getResult() {
        if (currentNumber == null) {
            return lastNumber.toString();
        }
        return currentNumber.toString();
    }

    public void setOperation(String operation) {
        evaluate();
        this.operation = operation;
    }
}
