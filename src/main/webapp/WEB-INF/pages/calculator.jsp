<html>
<head>

</head>
<body>
<h1>${message}</h1>

<h2>Calculator</h2>
<input id="result" readonly="readonly" value="${result}">
<table id="buttons">
    <tr>
        <td><input type="button" value="1"></td>
        <td><input type="button" value="2"></td>
        <td><input type="button" value="3"></td>
        <td><input type="button" value="+"></td>
    </tr>
    <tr>
        <td><input type="button" value="4"></td>
        <td><input type="button" value="5"></td>
        <td><input type="button" value="6"></td>
        <td><input type="button" value="-"></td>
    </tr>
    <tr>
        <td><input type="button" value="7"></td>
        <td><input type="button" value="8"></td>
        <td><input type="button" value="9"></td>
        <td><input type="button" value="*"></td>
    </tr>
    <tr>
        <td><input type="button" value="0"></td>
        <td><input type="button" value="C"></td>
        <td><input type="button" value="="></td>
        <td><input type="button" value="/"></td>
    </tr>
</table>
<br>
<a name="Survey" href="survey">Survey</a>
<script src="http://code.jquery.com/jquery-2.1.0.min.js"></script>
<script language="JavaScript">
    $(function () {
        $('#buttons [type="button"]').click(function () {
            var value = $(this).attr('value');
            $.get('./calculator.form', {button: value}).done(function (result) {
                $('#result').val(result);
            }).fail(function () {
                $('#result').val('<error!>');
            });
        });
//        $('h1').html('Hello World!');
    });
</script>
</body>
</html>